/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.game.frog.logic;

/**
 *  Singleton of the stones.
 *  the constructor empty prints a warning if stones are equals to ZERO
 * 
 *  This Class is responsible for storing only one instance of the stones.
 * 
 * @author Magno Cardona Heck
 */
public class SingletonStone {

    private static SingletonStone ref;
    private Stones stones;

    /**
     * Constructor of the singleton
     * it starts with an emptyhistoruy
     */
    private SingletonStone(int total) {
        setStones(new Stones(total));
    }

    /**
     * Get history
     * @return
     */
    public Stones getStones() {
        return this.stones;
    }

    /**
     * Sit a history
     * @param value
     */
    public void setStones(Stones value) {
        this.stones = value;
    }


    public static SingletonStone getSingleton() {
        if (ref == null) {
           System.out.print("ref is null create a singleton first with initial stones number");
        }
        return ref;
    }

    /**
     * The number of total sontes
     * @param totalStones
     * @return
     */
    public static SingletonStone getSingleton(int totalStones) {
        if (ref == null) {
            // it's ok, we can call this constructor
            ref = new SingletonStone(totalStones);
        }
        return ref;
    }
}
