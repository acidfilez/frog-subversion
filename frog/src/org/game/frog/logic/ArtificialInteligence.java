package org.game.frog.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This Class Is Responsible for the frog movement solution.
 * It will first detect that frogs going right like to be in the even
 * stones, left likes to be in odd positions stones. If the frogs going right
 * can be in a higher even position it will try to move or jump to it. 
 * If the frogs going left can be in a lower odd position it will try to 
 * move or jump to it. This will set frogs in the optimal positions, meaning
 * that we pair frogs going diferent direction in pair. 
 * (Ex. Left facing Right , Left facing Right, Left facing Right)
 * 
 * Then we take the frogs going right from the higher to lower and force the frog
 * to do any action Frog.doAnyAction() this will make the frog move or jump.
 * Then we take the frogs going left from the lower to higher and force the frog
 * to do any action Frog.doAnyAction() this will make the frog move or jump.
 * Finally when no more actions can be done the frogs have finish moving, and
 * they have all passed from one side to the another.
 * 
 * @author Magno Cardona Heck
 */
public class ArtificialInteligence {

    /**
     * This checks from the stone batch using the frog list
     * if any frog can perform an action (jump or move)
     * 
     * Returns true if at least one frog can do an action, false otherwise
     * 
     * @param stones the batch of stones
     * @param frogs the frogs list
     * @return boolean
     */
    public static boolean canMove(List frogs) {
        boolean status = false;

        //we clone stones, so we dont mess the original, while doing the test
        //its necesary to set loggging of the hisotry off.
        SingletonStone ss = SingletonStone.getSingleton();
        Stones originalStones = ss.getStones();
        Stones newStones = originalStones.clone();
        ss.setStones(newStones);
        newStones.setRecordHistory(false);

        //this will recursively call loopMove till no more moves can be done
        for (Iterator it = frogs.iterator(); it.hasNext();) {
            Frog frog = (Frog) it.next();
            if (frog != null) {
                status = status || frog.doAnyAction();
                if (status) {
                    break; //dont look for more
                }
            }
        }

        //Finaly we set the original sontes to the singleton.
        ss.setStones(originalStones);
        return status;
    }

    /**
     * the rules are  right frogs look for
     * the  left frogs look for the evens spots
     * after checking if they are in the even spot, they will
     * always try to jump to a bigger or smaller even or odd.
     * but to move to a less one, it will have to only jump.
     * @param frogs
     */
    public static void evenOddMoves(List frogs) {
        boolean didAction = true;
        while (didAction) {
            didAction = false;
            for (Iterator it = frogs.iterator(); it.hasNext();) {
                Frog frog = (Frog) it.next();
                boolean happyFrog = frog.isHappy();
                if (!happyFrog) {
                    if (frog.move()) {
                        didAction = true;

                    }
                }
            }

            for (Iterator it = frogs.iterator(); it.hasNext();) {
                Frog frog = (Frog) it.next();
                boolean happyFrog = frog.isEvenHappier();
                if (happyFrog) {
                    if (frog.jump()) {
                        didAction = true;
                    }

                }
            }
        }


        SingletonHistory sh = SingletonHistory.getSingleton();
        History lastHistory = sh.getHistory();
        //get list of hisotries, chechk if null
        //if null then create a clean arraylist
        List aHistories = sh.getHistories();
        if (aHistories == null) {
            aHistories = new ArrayList();
        }
        //add the lastHisotry
        aHistories.add(lastHistory);

        //set back the list of histories to the singleton
        sh.setHistories(aHistories);
    }

    /**
     * This method will try to perform an action to the rigth frogs decending
     * then left frog ascending. It will stop when no more actions are detected
     * @param frogs
     */
    public static void orderlyMove(List frogs) {
        int numberOfOneSideFrogs = frogs.size() / 2;
        boolean didAction = true;

        while (didAction) {
            didAction = false;
            int rightFrogs = numberOfOneSideFrogs;
            do {
                Frog frog = Frog.getFrogById(frogs, rightFrogs);
                if (frog.doAnyAction()) {
                    didAction = true;
                }
                rightFrogs--;
            } while (rightFrogs > 0);

            int leftFrogs = numberOfOneSideFrogs + 1;
            do {
                Frog frog = Frog.getFrogById(frogs, leftFrogs);
                if (frog.doAnyAction()) {
                    didAction = true;
                }
                leftFrogs++;
            } while (leftFrogs <= (numberOfOneSideFrogs * 2));
        }

    }
}
