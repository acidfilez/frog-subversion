/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.game.frog.logic;

import static java.lang.System.out;

import cl.bolchile.comun.util.StrUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author AcidFilez
 */
public class Stones {

    //array of stones. if frog stands on stone flag true is set.
    private boolean[] stones;    //store a list of frogs that exists.
    private Map hFrogs = new HashMap();    //history be recorded on the stones
    private boolean recordHistory = true;

    /**
     * Construct the stones that will existes
     * @param total
     */
    public Stones(int total) {
        boolean[] arrayBoolean = new boolean[total];
        for (int i = 0; i < total; ++i) {
            arrayBoolean[i] = false;
        }
        this.stones = arrayBoolean;
    }

    /**
     * return the total of stones
     * @return
     */
    public int getTotalStones() {
        if (this.stones != null) {
            return this.stones.length;
        }
        return 0;
    }

    /**
     * Is the stone ocupied ?
     * if a frog is standing on the stone, it will return true
     * 
     * @param position
     * @return
     */
    public boolean isStone(int position) {
        return this.stones[position - 1];
    }

    /**
     * Set a stone true or false. 
     * This method is used, when a frog stands on the stones
     * @param position
     * @param value
     */
    public void setStone(int position, boolean value) {
        this.stones[position - 1] = value;
    }

    /**
     * return the total of frogs
     * @return
     */
    public int getTotalFrogs() {
        if (this.stones != null) {
            return this.stones.length - 1;
        }
        return 0;
    }

    /**
     * the hFrogs contains index and an object frog
     * example: 1 will containg an object of frog.
     *          frog contains the position (stone 1) and his direction
     * @return the hFrogs a hashmap of frogs asociated to the stone
     *      */
    public Map getHFrogs() {
        return hFrogs;
    }

    /**
     * @param hFrogs the hFrogs to set
     */
    public void setHFrogs(Map hFrogs) {
        this.hFrogs = hFrogs;
    }

    /**
     * This method will asociate the created frogs agains the stones.
     * First the method will set all stones to false, occupied to false.
     * Then using the list of frogs created it will asociate the stone vs a frog
     *
     * @param stones stones batch
     * @param frogs the list of frogs
     */
    public static void associate(Stones stones, List frogs) {

        for (int x = 1; x <= stones.getTotalStones(); x++) {
            Stones.setStoneByPostion(x, false);
        }
        Map stonesVsFrog = new HashMap();
        for (Iterator it = frogs.iterator(); it.hasNext();) {
            Frog frog = (Frog) it.next();
            Stones.setStoneByPostion(frog.getPosition(), true);
            stonesVsFrog.put(frog.getPosition(), frog);
        }

        stones.setHFrogs(stonesVsFrog);
    }

    /**
     * Method will drow the stones and the frogs in screen
     * @param stones
     */
    public static void drawScreen(Stones stones) {

        Map frogStones = stones.getHFrogs();
        Frog myFrog = new Frog();
       
        int totalStones = stones.getTotalStones();

        for (int x = 1; x <= totalStones; x++) {
            myFrog = (Frog) frogStones.get(x);
            if (myFrog != null && stones.isStone(x)) {
                out.print(" " + Frog.getArrow(myFrog) + "   ");
            } else {
                out.print("     ");
            }
        }

        out.println("");
        out.println(StrUtil.repeat("___", totalStones, "  "));

        //print numbers
        for (int x = 1; x <= totalStones; x++) {
            out.print(" " + x + "   ");
        }
        out.println("");


    }

    /**
     * Util, this will set a stone value, depending on the position. 
     * 
     * @param stones batch of stones
     * @param position the stone position
     * @param value the boolean value it will set to that stone
     */
    public static void setStoneByPostion(int position, boolean value) {
        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();
        stones.setStone(position, value);
    }

    public static void takeSnapShoot() {
        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();
        //we now store the action we are going to perform
        SingletonHistory sh = SingletonHistory.getSingleton();
        History history = sh.getHistory();
        ArrayList listHistory = history.getAHistory();
        Map action = new HashMap();
        action.put("snapshot", stones.clone());
        listHistory.add(action);
    }

    /**
     * Util, this will set a stone value, depending on the position.
     *
     * @param stones batch of stones
     * @param position the stone position
     * @param value the boolean value it will set to that stone
     */
    public static void setStoneByPostion(Stones stones, int position, boolean value) {
        stones.setStone(position, value);
    }

    public static boolean isStoneXYZ(int position) throws Exception {
        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();
        return stones.isStone(position);
    }

    @Override
    public Stones clone() {
        //going to clone the stone object
        Stones newClone = new Stones(this.getTotalStones());
        Map newCloneFrogStonesClones = new HashMap();

        int totalStones = this.getTotalStones();
        //clean all the stones
        for (int x = 1; x <= totalStones; x++) {
            Stones.setStoneByPostion(newClone, x, false);
        }

        Frog myFrog = new Frog();
        Map frogStones = this.getHFrogs();

        int totalFrogs = frogStones.size();

        for (int x = 1; x <= totalFrogs + 1; x++) {
            myFrog = (Frog) frogStones.get(x);
            if (myFrog != null) {
                Stones.setStoneByPostion(newClone, myFrog.getPosition(), true);
                newCloneFrogStonesClones.put(myFrog.getPosition(), myFrog.clone());
            }

        }

        newClone.setHFrogs(newCloneFrogStonesClones);

        return newClone;
    }

    /**
     * @return the recordHistory
     */
    public boolean isRecordHistory() {
        return recordHistory;
    }

    /**
     * @param recordHistory the recordHistory to set
     */
    public void setRecordHistory(boolean recordHistory) {
        this.recordHistory = recordHistory;
    }
}
