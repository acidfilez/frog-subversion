package org.game.frog.logic;

import java.util.List;

/**
 *  Singleton of the actual frog history.
 *  This Class can hold one or more history instances, and be store in the 
 *  List histories. Keep in mind that each history contains its own frog actions. 
 * 
 * @author Magno Cardona Heck
 */
public class SingletonHistory {

    private static SingletonHistory ref;
    private History resource;
    /** Store all posible History instances, it normally stores only one */
    private List histories;
    /** Number of history to go back to */
    private int timeBack = 1; 
    

    /**
     * Constructs the singleton pattern
     * It will use a existing history to create
     * the singleton
     * @param resource
     */
    public SingletonHistory(History resource) {
        this.resource = resource;
    }
    
    /**
     * Constructor of the singleton
     * it starts with an emptyhistoruy
     */
    private SingletonHistory() {
        setHistory(new History());
    }

    /**
     * Get history
     * @return
     */    
    public History getHistory() {
        return this.resource;
    }

    /**
     * Sit a history
     * @param value
     */
    public void setHistory(History value) {
        this.resource = value;
    }

    
    
    /**
     * Gets the singleton instance
     * @return
     */
    public static SingletonHistory getSingleton() {
        if (ref == null) {
            // it's ok, we can call this constructor
            ref = new SingletonHistory();
        }
        return ref;
    }

    /**
     * This will return all histories before they are rolled back
     * @return the histories
     */
    public List getHistories() {
        return histories;
    }

    /**
     * This will add a  new history to the histories before they are rolled back
     * @param histories the histories to set
     */
    public void setHistories(List histories) {
        this.histories = histories;
    }

    /**
     * get the number of actions it has to go back in time
     * @return
     */
    public int getTimeBack() {
        return timeBack;
    }

    
    /**
     * set the number of actions it has to go back in time
     * @return
     */
    public void setTimeBack(int timeBack) {
        this.timeBack = timeBack;
    }
}
