/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.game.frog.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

/**
 * This class creates a Frog Logic Object. We Store the frogs id(name) its
 * position, direction, and its last position.
 * 
 * This class makes the frog to move, jump, or do any of those.
 * It can also check if the frog is using a stone. Or it will check the level
 * of happiness of the frog. 
 * 
 * Frogs are happy if they are in even or odd position, its even happier if
 * its in a higer or lower, even or odd, position.
 * 
 * It also contains utility method, such as create the necesary frogs and store
 * it to a List of frogs. Can check if is in odd or even position. An utility
 * method to clone itself, and finally a method that return a frog from the
 * frog list by id.
 * 
 * 
 * @author Magno Cardona Heck
 */
public class Frog {

    /**
     * Frogs properties
     */
    private int id;
    private int position;
    private int hisotry;
    private String direction; //right or left
    /**
     * Constant of the direction
     */
    public static final String RIGHT = "right";
    public static final String LEFT = "left";
    public static final String BOTH = "both";

    //                              Constructors                              // 
    /**
     * Constructor of the frog
     */
    public Frog () {
        this.setId(0);
        this.setPosition(1);
        this.setDirection(Frog.BOTH);

    }

    /**
     * It will construct the frog, it needs
     * the stone where its is standing, and the direction
     * the frog will move or jump (Frog.RIGHT or Frog.LEFT)
     * @param position
     * @param direction
     */
    public Frog (int id, int position, String direction) {
        this.setId(id);
        this.setPosition(position);
        if (Frog.RIGHT.equals(direction)) {
            this.setDirection(Frog.RIGHT);
        }
        if (Frog.LEFT.equals(direction)) {
            this.setDirection(Frog.LEFT);
        }
    }

    //                              Frogs Movements                           // 
    /***
     * It will try to move the frog. It will check if
     * the frog has an open stone infront of him
     * @param stones
     */
    public boolean move () {
        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();

        int tokenPostion = 0;

        if (Frog.RIGHT.equals(this.direction)) {
            tokenPostion =  + 1;
        }

        if (Frog.LEFT.equals(this.direction)) {
            tokenPostion = -1;
        }

        int newPosition = this.position + tokenPostion;


        Map hStones = stones.getHFrogs();

        if ((newPosition >= 1) && (newPosition <= stones.getTotalStones()) &&
                 ! this.isOccupied(stones, newPosition)) {
            //cant record hisotry
            //stop here and return it can doAction, but actully not do it
            if ( ! stones.isRecordHistory()) {
                return true;
            }

            //remember where was I initial
            this.setHisotry(this.position);

            //clear old position stones
            hStones.put(this.position, null);
            Stones.setStoneByPostion(this.position, false);

            this.position = newPosition;
            hStones.put(this.position, this.clone());
            Stones.setStoneByPostion(this.position, true);

            ss.setStones(stones);


            //we now store the action we are going to perform
            SingletonHistory sh = SingletonHistory.getSingleton();
            History history = sh.getHistory();
            ArrayList listHistory = history.getAHistory();
            Map action = new HashMap();
            action.put("MOVE", this.clone());
            action.put("snapshot", stones.clone());
            listHistory.add(action);
            return true;
        }
        return false;
    }

    /**
     * It wil make the frog jump. It will depends if the
     * frog infront of him is of oposite direction and
     * will check if the frog has an open stone imidiatly after the frog
     * that is trying to jump
     *
     * @param stones
     */
    public boolean jump () {
        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();

        int tokenPostion = 0;
        int tokenPostion1 = 0;

        if (Frog.RIGHT.equals(this.direction)) {
            tokenPostion =  + 2;
            tokenPostion1 =  + 1;
        }
        if (Frog.LEFT.equals(this.direction)) {
            tokenPostion = -2;
            tokenPostion1 = -1;
        }

        int newPosition = this.position + tokenPostion;
        int newPosition1 = this.position + tokenPostion1;

        Map hStones = stones.getHFrogs();
        Frog nextFrog = (Frog) hStones.get(newPosition1);

        if ((newPosition >= 1) && (newPosition <= stones.getTotalStones()) && //in the stone ranges
                 ! this.isOccupied(stones, newPosition) && //new position is empty
                (this.isOccupied(stones, newPosition1)) && //a frog to jump
                //the frog to jump has to be of oppositive direction.
                (nextFrog != null &&  ! nextFrog.getDirection().equals(this.direction))) {

            //cant record hisotry
            //stop here and return it can doAction, but actully not do it
            if ( ! stones.isRecordHistory()) {
                return true;
            }

            this.setHisotry(this.position);

            //we clear the stones position
            hStones.put(this.position, null);
            Stones.setStoneByPostion(this.position, false);

            this.position = newPosition;
            hStones.put(newPosition, this.clone());
            Stones.setStoneByPostion(this.position, true);

            ss.setStones(stones);
            //we now store the action we are going to perform
            SingletonHistory sh = SingletonHistory.getSingleton();
            History history = sh.getHistory();
            ArrayList listHistory = history.getAHistory();
            Map action = new HashMap();
            action.put("JUMP", this.clone());
            action.put("snapshot", stones.clone());
            listHistory.add(action);
            return true;
        }
        return false;
    }

    /**
     * This will try to jump first then move else, does nothing
     * @param stones
     */
    public boolean doAnyAction () {
        //remember my initial position
        int oldPosition = this.position;
        //jump first
        if (this.jump()) {
            return true;
        }
        if (oldPosition == this.position) {
            if (this.move()) {
                return true;
            }
        }
        return false;
    }
    
    //                              Methods                                   // 
    /**
     * Will check if the stone is occupied
     *
     * @param stones batch
     * @param position
     * @return
     */
    public boolean isOccupied (Stones stones, int position) {
        boolean isBusy = stones.isStone(position);
        return isBusy;
    }

    /**
     * Method will return true if the position of the frog is odd
     * @return
     */
    public boolean isOddPosion () {
        if (this.position % 2 != 0) {
            return true;
        }
        return false;
    }

    /**
     * Method will return true if the position is even.
     * @return
     */
    public boolean isEvenPosion () {
        if (this.position % 2 == 0) {
            return true;
        }
        return false;
    }

    //                              Frogs AI Methods                          // 
    /**
     * Method will return true, if the frog is happy.
     * Frogs with Direction Right are happy when they are standing in an odd Position
     * Frogs with Direction Left are happy when they are standing in an even Position
     * @return
     */
    public boolean isHappy () {
        //check the direction of the frog
        if (Frog.RIGHT.equals(this.direction)) { //going right
            if (this.isEvenPosion()) { //Es Par
                return true;
            }
        } else {//going left
            if (this.isOddPosion()) { //Es Impar
                return true;
            }
        }
        //anything else, its just an unhappy frog.
        return false;
    }

    /**
     * Method will check if for a stone that is empty
     * and the frog feels happy to jump to it.
     * @return
     */
    public boolean isEvenHappier () {
        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();

        //check the direction of the frog
        if (Frog.RIGHT.equals(this.direction)) { //going right
            //get the happier position
            try {
                //get the happier position
                int newPosition = this.position + 2;

                //new positon is offset
                if ( ! (newPosition >= 1 && newPosition <= stones.getTotalStones())) {
                    return false;
                }
                //is the newPosition Even, if so, we can keep checking the rest
                if ( ! Frog.isEven(newPosition)) {
                    return false;
                }
                //check if that stone is empty
                boolean stoneOccupied = Stones.isStoneXYZ(newPosition);
                if (stoneOccupied) { //if true, the is occupied
                    //if occupied, frog is not happier, cause it cant jump
                    return false;
                }
            } catch (Exception ex) {
                System.out.println("error:" + ex.getMessage());
            }
        } else {//going left
            try {
                //get the happier position
                //new positon is offset
                int newPosition = this.position - 2;
                if ( ! (newPosition >= 1 && newPosition <= stones.getTotalStones())) {
                    return false;
                }
                if ( ! Frog.isOdd(newPosition)) {
                    return false;
                }
                //check if that stone is empty
                boolean stoneOccupied = Stones.isStoneXYZ(newPosition);
                if (stoneOccupied) {
                    return false;
                }
            } catch (Exception ex) {
                System.out.println("error:" + ex.getMessage());
            }
        }
        //anything else, it will jump and be even happier
        return true;
    }

    //                              Utility  Methods                          //
    /**
     * This method will create all the necesary frogs
     * @param frogs
     * @param numberOfOneSideFrogs
     */
    public static List createFrogs (int numberOfOneSideFrogs) {
        List frogs = new ArrayList();
        int totalOfFrogs = numberOfOneSideFrogs * 2;

        for (int counter = 1; counter <= numberOfOneSideFrogs; counter ++) {
            Frog singleFrogRight = new Frog(counter, counter, Frog.RIGHT);
            frogs.add(singleFrogRight);
        }
        for (int counter = numberOfOneSideFrogs + 1; counter <= totalOfFrogs; counter ++) {
            Frog singleFrogLeft = new Frog(counter, counter + 1, Frog.LEFT);
            frogs.add(singleFrogLeft);
        }
        return frogs;
    }

    /**
     * return a frog from the list identify by id
     * @param frogs
     * @return
     */
    public static Frog getFrogById (List frogs, int id) {
        for (Iterator it = frogs.iterator(); it.hasNext();) {
            Frog frog = (Frog) it.next();
            if (frog.getId() == id) {
                return frog;
            }
        }
        return null;
    }
    
    //                              Presentation Methods                      //   
    public static String getArrow (Frog frog) {
        if (frog == null) {
            return " ";
        }
        if (Frog.RIGHT.equals(frog.getDirection())) {
            return ">";
        } else {
            return "<";
        }
    }
    
    //                              Getters and Settes                        // 
    /**
     * @return the direction
     */
    public String getDirection () {
        return direction;
    }

    /**
     * Set the direction where it will go
     * @param direction
     */
    public void setDirection (String direction) {
        this.direction = direction;
    }

    /**
     * @return the hisotry
     */
    public int getHisotry () {
        return hisotry;
    }

    /**
     * @param hisotry the hisotry to set
     */
    public void setHisotry (int hisotry) {
        this.hisotry = hisotry;
    }

    /**
     * @return the position
     */
    public int getPosition () {
        return this.position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition (int position) {
        this.position = position;
    }

    /**
     * @return the position
     */
    public int getId () {
        return this.id;
    }

    /**
     * @param position the position to set
     */
    public void setId (int id) {
        this.id = id;
    }

    //                              Cloning Frogs                             // 
    /**
     *  Clones the object, through dificult ADN trial and error
     */
    @Override
    public Frog clone () {
        Frog myFrogClone = new Frog(this.getId(), this.getPosition(), this.getDirection());
        myFrogClone.setHisotry(this.hisotry);
        return myFrogClone;
    }

    //                              Static Methods Utilities                  // 
    private static boolean isEven (int position) {
        if (position % 2 == 0) {
            return true;
        }
        return false;
    }

    private static boolean isOdd (int position) {
        if (position % 2 != 0) {
            return true;

        }
        return false;
    }
}
