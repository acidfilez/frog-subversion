/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.game.frog.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This Class is responsible for storing all the posible frogs actions done in
 * the game. It will store jumps, moves in an array list.
 * 
 * In the SingletonHistory can also be used to store one history, it can store
 * many Hisotory Instances. 
 * 
 * This Class has utility method to print the actions of the history in the
 * console, or prints all hisotories that can be found. Finally has a utility
 * to clone the hisotory.
 *
 * @author Magno Cardona Heck
 */
public class History {

    private ArrayList aHistory = new ArrayList();

    /**
     * @return the aHistory
     */
    public ArrayList getAHistory() {
        return aHistory;
    }

    /**
     * @param aHistory the aHistory to set
     */
    public void setAHistory(ArrayList aHistory) {
        this.aHistory = aHistory;
    }

    /**
     * Get back the number of actions a history has
     * @return
     */
    public int getAHistorySize() {
        return aHistory.size();
    }

    /**
     * Method will print the histogram of actions a frog history
     */
    public void print() {
        for (Iterator it = this.aHistory.iterator(); it.hasNext();) {
            Map action = (HashMap) it.next();
            //check if the action has a JUMP, OR MOVE
            Frog frogJump = (Frog) action.get("JUMP");
            Frog frogMove = (Frog) action.get("MOVE");
            Stones stoneSnapShot = (Stones) action.get("snapshot");

            if (frogJump != null) {
                //frog jumped
                System.out.println("Frog id: " + frogJump.getId() +
                        " jumped from position:" + frogJump.getHisotry() +
                        " to:" + frogJump.getPosition() +
                        " in direction: " + frogJump.getDirection());
            }

             if (frogMove != null) {
                System.out.println("Frog id: " + frogMove.getId() +
                        " moved from position:" + frogMove.getHisotry() +
                        " to:" + frogMove.getPosition() +
                        " in direction:" + frogMove.getDirection());
            }

            Stones.drawScreen(stoneSnapShot);

            System.out.println("");
            System.out.println("");

        }

    }

    /**
     * This method will print all hisotories present, frogs can have many histories
     * each history has actions that will be print to screen
     */
    public void printHistories() {
        //System.out.println("history is:"+aHistory);
        SingletonHistory sh = SingletonHistory.getSingleton();
        List aHistories = sh.getHistories();
        if (aHistories == null) {
            aHistories = new ArrayList();
        }
        System.out.println("");
        System.out.println("Total Hisotories in frogs:" + aHistories.size());
        System.out.println("");

        int counter = 0;
        for (Iterator iteratorHisotory = aHistories.iterator(); iteratorHisotory.hasNext();) {
            counter++;
            System.out.println("Hisotory #:" + counter);
            History history = (History) iteratorHisotory.next();
            List aList = history.getAHistory();
            for (Iterator iteratorActions = aList.iterator(); iteratorActions.hasNext();) {
                Map action = (HashMap) iteratorActions.next();
                //check if the action has a JUMP, OR MOVE
                Frog frogJump = (Frog) action.get("JUMP");
                Frog frogMove = (Frog) action.get("MOVE");
                Stones stoneSnapShot = (Stones) action.get("snapshot");

                if (frogJump != null) {
                    //frog jumped
                    System.out.println("Frog id: " + frogJump.getId() +
                            " jumped from position:" + frogJump.getHisotry() +
                            " to:" + frogJump.getPosition() +
                            " in direction: " + frogJump.getDirection());
                } else {
                    System.out.println("Frog id: " + frogMove.getId() +
                            " moved from position:" + frogMove.getHisotry() +
                            " to:" + frogMove.getPosition() +
                            " in direction:" + frogMove.getDirection());
                }

                Stones.drawScreen(stoneSnapShot);
                System.out.println("");
                System.out.println("");
            }
        }
    }

    @Override
    /**
     * Clone the history
     */
    public History clone() {
        History newClone = new History();
        List actionsClone = new ArrayList();

        List oldActions = this.getAHistory();
        if (oldActions!=null){
            for (Iterator it = oldActions.iterator(); it.hasNext();) {
                Map action = (HashMap) it.next();
                Map actionClone = new HashMap();
                Frog frogJump = (Frog) action.get("JUMP");
                Frog frogMove = (Frog) action.get("MOVE");
                Stones stoneSnapShot = (Stones) action.get("snapshot");

                if (frogJump != null) {
                    //frog jumped
                    actionClone.put("JUMP", frogJump.clone());
                   
                } else {
                   //frog moved
                    actionClone.put("MOVE", frogMove.clone());
                }
                 actionClone.put("snapshot", stoneSnapShot);
                actionsClone.add(actionClone);
            }
        }
        newClone.setAHistory((ArrayList) actionsClone);

        return newClone;
    }
}
