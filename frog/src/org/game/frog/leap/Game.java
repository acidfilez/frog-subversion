package org.game.frog.leap;

import java.awt.AWTEvent;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.game.entities.Entity;
import org.game.entities.FrogEntity;
import org.game.entities.StoneEntity;
import org.game.frog.logic.ArtificialInteligence;
import org.game.frog.logic.Frog;
import org.game.frog.logic.History;
import org.game.frog.logic.SingletonHistory;
import org.game.frog.logic.SingletonStone;
import org.game.frog.logic.Stones;

/**
 * The main hook of our game. This class with both act as a manager
 * for the display and central mediator for the game logic. 
 * 
 * Display management will consist of a loop that cycles round all
 * entities in the game asking them to move and then drawing them
 * in the appropriate place. With the help of an inner class it
 * will also allow the player to control the frog movements.
 * 
 * As a mediator it will be informed when entities within our game
 * detect events (e.g.any event that the game has) and will take
 * appropriate game actions.
 * 
 * @author Kevin Glass / Magno Cardona
 */
public class Game extends Canvas {

    private static final long serialVersionUID = 4469474620579584042L;
    /**  The width of the window, it will change depending the number of frogs */
    private int WINDOW_WIDTH = 930;
    /**  The height of the window, it will change depending the number of frogs */
    private int WINDOW_HEIGHT = 600;
    /** Is the game still running, setting this to false will end game.  */
    private boolean gameRunning = true;
    /** The stragey that allows us to use accelerate page flipping */
    private BufferStrategy strategy;
    /** The list of all the entities that exist in our game */
    private List entities = new ArrayList();
    /** The list of entities that need to be removed from the game this loop */
    private List removeList = new ArrayList();
    /** True if we're holding up game play until a key has been pressed */
    private boolean waitingForKeyPress = true;
    /** This is the number of the history action we are currently observing */
    public int numberSnapShoot = 1;
    private JFrame container = new JFrame("Frog Leap v1");

    /**
     * The main hook of our game. This class with both act as a manager
     * for the display and central mediator for the game logic. 
     * 
     * Display management will consist of a loop that cycles round all
     * entities in the game asking them to move and then drawing them
     * in the appropriate place. With the help of an inner class it
     * will also allow the player to control the keyboard action.
     * 
     * As a mediator it will be informed when entities within our game
     * detect events (e.g. alient killed, played died) and will take
     * appropriate game actions.
     * 
     * @author Kevin Glass / Magno Cardona Heck
     */
    public Game() {
        // create a frame to contain our game

        // get hold the content of the frame and set up the resolution of the game
        JPanel panel = (JPanel) container.getContentPane();
        panel.setPreferredSize(new Dimension(this.WINDOW_WIDTH, this.WINDOW_HEIGHT));
        panel.setLayout(null);

        panel.add(this);

        // Tell AWT not to bother repainting our canvas since we're
        // going to do that our self in accelerated mode
        setIgnoreRepaint(true);

        // finally make the window visible
        container.pack();
        container.setResizable(false);
        container.setVisible(true);

        String numberOfFrogsStr = (String) JOptionPane.showInputDialog(
                container,
                "How Many Frogs Do You Want?",
                "Enter the number of frogs",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                null);

        int numberOfFrogs = 4;

        try {
            numberOfFrogs = Integer.valueOf(numberOfFrogsStr);
        } catch (Exception e) {
        }

        Object[] options = {"Yes, play ?", "No, Solve Game Now!"};
        int playOrShow = JOptionPane.showOptionDialog(container,
                "Do you want to play, or show the frog jump solution?",
                "Before you Start",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, //do not use a custom Icon
                options, //the titles of buttons
                options[0]); //default button title

        if (playOrShow == JOptionPane.YES_OPTION) {
            System.out.println("Playing Game");
            Game.playGame(numberOfFrogs);
            detectMouseToPlay();
        } else {
            System.out.println("Showing Game Solution");
            //initialize variables to show the solution.
            Game.showSolution(numberOfFrogs);
            //add mouse listeners, on click moves the frogs.
            detectMouseToShow();
        }

        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();

        int totalStones = stones.getTotalStones();
        // 930- 6
        // x  - 8


        //Re calculate widht with more frogs.
        int newDynaWidht = 130 * totalStones + 12;
        this.WINDOW_WIDTH = newDynaWidht;

        // get hold the content of the frame and set up the resolution of the game
        panel = (JPanel) container.getContentPane();
        container.setSize(new Dimension(this.WINDOW_WIDTH, this.WINDOW_HEIGHT));
        panel.setPreferredSize(new Dimension(this.WINDOW_WIDTH, this.WINDOW_HEIGHT));
        panel.setLayout(null);

        panel.setAutoscrolls(true);

        // setup our canvas size and put it into the content of the frame
        setBounds(0, 0, this.WINDOW_WIDTH, this.WINDOW_HEIGHT);

        // add a listener to respond to the user closing the window. If they
        // do we'd like to exit the game
        container.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // add a key input system (defined below) to our canvas
        // so we can respond to key pressed
        addKeyListener(new KeyInputHandler());

        // create the buffering strategy which will allow AWT
        // to manage our accelerated graphics
        createBufferStrategy(2);
        strategy = getBufferStrategy();

        // initialise the entities in our game so there's something
        // to see at startup
        initEntities(this.numberSnapShoot);

    }

    /**
     * The main game loop. This loop is running during all game
     * play as is responsible for the following activities:
     * <p>
     * - Working out the speed of the game loop to update moves
     * - Moving the game entities
     * - Drawing the screen contents (entities, text)
     * - Updating game events
     * - Checking Input
     * <p>
     */
    public void gameLoop() {
        long lastLoopTime = System.currentTimeMillis();

        // keep looping round til the game ends
        while (this.gameRunning) {
            // work out how long its been since the last update, this
            // will be used to calculate how far the entities should
            // move this loop
            long delta = System.currentTimeMillis() - lastLoopTime;
            lastLoopTime = System.currentTimeMillis();

            // Get hold of a graphics context for the accelerated
            // surface and blank it out
            Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
            g.setColor(Color.black);
            g.fillRect(0, 0, this.WINDOW_WIDTH, this.WINDOW_HEIGHT);


            // cycle round drawing all the entities we have in the game
            for (int i = 0; i < entities.size(); i++) {
                Entity entity = (Entity) entities.get(i);
                entity.draw(g);
            }

            // remove any entity that has been marked for clear up
            entities.removeAll(removeList);
            removeList.clear();

            // finally, we've completed drawing so clear up the graphics
            // and flip the buffer over
            g.dispose();
            strategy.show();

            // finally pause for a bit. Note: this should run us at about
            // 100 fps but on windows this might vary each loop due to
            // a bad implementation of timer
            try {
                Thread.sleep(10);
            } catch (Exception e) {
            }

        }
    }

    private void initEntities(int numberSnapshoot) {
        // boxes dimeniton 128x128  + 12 = 140
        // frog  dimeniton 80x80
        // start boxes at 130 + 80 = 210

        SingletonHistory sh = SingletonHistory.getSingleton();
        History history = sh.getHistory();
        ArrayList aHistory = history.getAHistory();

        SingletonStone ss = SingletonStone.getSingleton();
        Stones stones = ss.getStones();

        int totalFrogs = stones.getTotalFrogs();
        int totalStones = stones.getTotalStones();


        for (int stonesCounter = 0; stonesCounter < totalStones; stonesCounter++) {
            int newX = 130 * stonesCounter;
            Entity stone = new StoneEntity(this, "org/game/frog/sprites/resources/box.gif", 12 + newX, 210, stonesCounter);
            entities.add(stone);
        }

        //drow first snapshoot, this is the initial drawing
        int counter = 0;
        for (Iterator it = aHistory.iterator(); it.hasNext();) {
            Map action = (HashMap) it.next();
            counter++;
            if (counter == numberSnapshoot) {
                //check if the action has a JUMP, OR MOVE
                Stones stoneSnapShot = (Stones) action.get("snapshot");
                Map frogStones = stoneSnapShot.getHFrogs();
                Frog myFrog;
                for (int frogsCounter = 1; frogsCounter <= totalStones; frogsCounter++) {
                    int newX = 130 * (frogsCounter - 1);
                    myFrog = (Frog) frogStones.get(frogsCounter);
                    Entity frog = null;
                    if (myFrog != null && stoneSnapShot.isStone(frogsCounter)) {
                        if (Frog.LEFT.equals(myFrog.getDirection())) {
                            frog = new FrogEntity(this,
                                    "org/game/frog/sprites/resources/frogLeft.gif",
                                    35 + newX, 180, frogsCounter);
                        }
                        if (Frog.RIGHT.equals(myFrog.getDirection())) {
                            frog = new FrogEntity(this,
                                    "org/game/frog/sprites/resources/frogRight.gif",
                                    35 + newX, 180, frogsCounter);
                        }
                        entities.add(frog);
                    } else {
                        //ignore the frog, will leave emtpy space, because of newX;
                    }
                }
            }
        }
    }

    /**
     * A class to handle keyboard input from the user. The class
     * handles both dynamic input during game play, i.e. left/right
     * and shoot, and more static type input (i.e. press any key to
     * continue)
     *
     * This has been implemented as an inner class more through
     * habbit then anything else. Its perfectly normal to implement
     * this as seperate class if slight less convienient.
     *
     * @author Kevin Glass
     */
    private class KeyInputHandler extends KeyAdapter {

        /**
         * Notification from AWT that a key has been pressed. Note that
         * a key being pressed is equal to being pushed down but *NOT*
         * released. Thats where keyTyped() comes in.
         *
         * @param e The details of the key that was pressed
         */
        @Override
        public void keyPressed(KeyEvent e) {
            // if we're waiting for an "any key" typed then we don't
            // want to do anything with just a "press"
            if (waitingForKeyPress) {
                return;
            }

            // Example of the left keypress
            //            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            //                leftPressed = true;
            //            }

        }

        /**
         * Notification from AWT that a key has been released.
         *
         * @param e The details of the key that was released
         */
        @Override
        public void keyReleased(KeyEvent e) {
            // if we're waiting for an "any key" typed then we don't
            // want to do anything with just a "released"
            if (waitingForKeyPress) {
                return;
            }
            //Example of the left keypress
            //  if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            //        leftPressed = false;
            //  }

        }

        /**
         * Notification from AWT that a key has been typed. Note that
         * typing a key means to both press and then release it.
         *
         * @param e The details of the key that was typed.
         */
        @Override
        public void keyTyped(KeyEvent e) {

            // if we hit escape, then quit the game
            if (e.getKeyChar() == 27) {
                System.exit(0);
            }

            // if we hit space, move next snapshot
            if (e.getKeyChar() == 32) {
                //clear all entities
                entities.clear();
                //get the next snapShoot
                numberSnapShoot++;

                //end game
                SingletonHistory sh = SingletonHistory.getSingleton();
                History history = sh.getHistory();
                int historySize = history.getAHistorySize();
                if (numberSnapShoot > historySize) {
                    System.exit(0);
                }
                //draw entities, depending of the snapshoot
                initEntities(numberSnapShoot);
            }
        }
    }

    /**
     * The entry point into the game. We'll simply create an
     * instance of class which will start the display and game
     * loop.
     *
     * @param argv The arguments that are passed into our game
     */
    public static void main(String argv[]) {
        Game g = new Game();

        // Start the main game loop, note: this method will not
        // return until the game has finished running. Hence we are
        // using the actual main thread to run the game.
        g.gameLoop();
    }

    private static void showSolution(int numberOfOneSideFrogs) {

        /****************** dont modfied below this point *************/
        int totalOfStones = (numberOfOneSideFrogs * 2) + 1;

        //create a singleto of stones
        SingletonStone ss = SingletonStone.getSingleton(totalOfStones);
        Stones stones = ss.getStones();

        //Creat the left and right frogs
        List frogs = Frog.createFrogs(numberOfOneSideFrogs);

        //finally we associate the stones with the frogs
        //stones contains a hasmap of stones vs frogs.
        Stones.associate(stones, frogs);

        //we set the singletons, with the new stones an assoiactions
        ss.setStones(stones);

        //Take the initial snapShoot
        Stones.takeSnapShoot();

        //AI will start moving the the positions of the frogs
        //are happiers
        ArtificialInteligence.evenOddMoves(frogs);
        //Now do actions with right frogs, left frogs
        //it will stop until it cant move any frogs
        //doAnyAction does (first move, then jump)
        ArtificialInteligence.orderlyMove(frogs);

        System.out.println("Frogs last snapshot:");
        Stones.drawScreen(stones);
        System.out.println("");
        System.out.println("");

        //check the frog history moves
        System.out.println("Frogs and Stones Histogram");
        SingletonHistory sh = SingletonHistory.getSingleton();
        History history = sh.getHistory();
        history.print();
    }

    private static void playGame(int numberOfOneSideFrogs) {

        /****************** dont modfied below this point *************/
        int totalOfStones = (numberOfOneSideFrogs * 2) + 1;

        //create a singleto of stones
        SingletonStone ss = SingletonStone.getSingleton(totalOfStones);
        Stones stones = ss.getStones();

        //Creat the left and right frogs
        List frogs = Frog.createFrogs(numberOfOneSideFrogs);

        //finally we associate the stones with the frogs
        //stones contains a hasmap of stones vs frogs.
        Stones.associate(stones, frogs);

        //we set the singletons, with the new stones an assoiactions
        ss.setStones(stones);

        //Take the initial snapShoot
        Stones.takeSnapShoot();

    }

    private void detectMouseToShow() {
        long eventMask = AWTEvent.MOUSE_EVENT_MASK;

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent evt) {

                if (evt.getID() == java.awt.Event.MOUSE_DOWN) {
                    //clear all entities
                    entities.clear();
                    //get the next snapShoot
                    numberSnapShoot++;

                    //end game
                    SingletonHistory sh = SingletonHistory.getSingleton();
                    History history = sh.getHistory();
                    int historySize = history.getAHistorySize();
                    if (numberSnapShoot > historySize) {
                        System.exit(0);
                    }
                    //draw entities, depending of the snapshoot
                    initEntities(numberSnapShoot);
                }
            }
        }, eventMask);
    }

    private void detectMouseToPlay() {
        long eventMask = AWTEvent.MOUSE_EVENT_MASK;

        Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {

            public void eventDispatched(AWTEvent evt) {
                List frogs = new ArrayList();

                SingletonStone ss = SingletonStone.getSingleton();
                Stones stones = ss.getStones();
                Map hStones = stones.getHFrogs();

                Frog frog;
                Iterator it = hStones.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    frog = (Frog) pairs.getValue();
                    frogs.add(frog);
                }



                if (evt.getID() == java.awt.Event.MOUSE_DOWN) {
                    MouseEvent mevt = (MouseEvent) evt;
                    Point p = mevt.getPoint();

                    boolean didAction = false;


                    // cycle round drawing all the entities we have in the game
                    for (int i = 0; i < entities.size(); i++) {
                        Entity entity = (Entity) entities.get(i);
                        if (entity instanceof FrogEntity) {

//                            //cycle the frogs map and check if the frog in the map match
                            if ((p.getX() >= entity.getX()
                                    && (p.getX() <= entity.getX() + 80))
                                    && (p.getY() >= entity.getY()
                                    && (p.getY() <= entity.getY() + 80))) {
//                                System.out.println("click on a freaking frog x:"
//                                        + entity.getX() + " y:" + entity.getY());


                                 frog = (Frog) hStones.get(entity.getId());
                                //move or jump this frog.

                                didAction = frog.doAnyAction();

                                if (didAction) {
                                    SingletonHistory sh = SingletonHistory.getSingleton();
                                    History lastHistory = sh.getHistory();
                                    //get list of hisotries, chechk if null
                                    //if null then create a clean arraylist
                                    List aHistories = sh.getHistories();
                                    if (aHistories == null) {
                                        aHistories = new ArrayList();
                                    }
                                    //add the lastHisotry
                                    aHistories.add(lastHistory);

                                    //set back the list of histories to the singleton
                                    sh.setHistories(aHistories);
                                }
                                break; //stop for.
                            }
                        }
                    }

                    if (didAction) {
                        //clear all entities
                        entities.clear();
                        //get the next snapShoot
                        numberSnapShoot++;
                        //draw entities, depending of the snapshoot
                        initEntities(numberSnapShoot);
                    }
//



//                  //Check if frogs are stucked.
                    boolean anyAction = ArtificialInteligence.canMove(frogs);

//
//                    //end game now
                    if (!anyAction) {
                        JOptionPane.showMessageDialog(container,
                                "You Loose",
                                "Bye",
                                JOptionPane.ERROR_MESSAGE);
                        System.exit(0);
                    }

                }
            }
        }, eventMask);
    }
}
