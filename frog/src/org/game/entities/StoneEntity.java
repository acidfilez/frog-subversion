package org.game.entities;

import org.game.frog.leap.Game;

/**
 * The entity that represents a stone
 * 
 * @author Kevin Glass / Magno Cardona
 */
public class StoneEntity extends Entity {

    /** The game in which the Stone exists */
    private Game game;

    /**
     * Create a new entity to represent the players Stone
     *
     * @param game The game in which the Stone is being created
     * @param ref The reference to the sprite to show for the Stone
     * @param x The initial x location of the player's Stone
     * @param y The initial y location of the player's Stone
     */
    public StoneEntity(Game game, String ref, int x, int y, int id) {
        super(ref, x, y, id);

        this.game = game;
    }
}