package org.game.entities;

import org.game.frog.leap.Game;

/**
 * The entity that represents the players frrog
 * 
 * @author Kevin Glass / Magno Cardona
 */
public class FrogEntity extends Entity {

    /** The game in which the Frog exists */
    private Game game;
   
    /**
     * Create a new entity to represent the players Frog
     *
     * @param game The game in which the Frog is being created
     * @param ref The reference to the sprite to show for the Frog
     * @param x The initial x location of the player's Frog
     * @param y The initial y location of the player's Frog
     */
    public FrogEntity(Game game, String ref, int x, int y, int id) {
        super(ref, x, y, id);
        this.game = game;
    }
}
