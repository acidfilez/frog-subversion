/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package frog;

import org.game.frog.logic.*;
import java.util.List;
import org.game.frog.leap.Game;

/**
 *
 * @author AcidFilez
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {


//        System.out.println("Frogs Complete Hisotories");
//        history.printHistories();

        // Start the main game loop, note: this method will not
        // return until the game has finished running. Hence we are
        // using the actual main thread to run the game.
        Game g = new Game();
        g.gameLoop();
    }
}
